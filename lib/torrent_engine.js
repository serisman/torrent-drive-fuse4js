var events = require('events');

var torrentStream = require('torrent-stream');

function TorrentEngine(torrent, logger) {
  var self = this;

  var _isLoaded = false;
  var _engine;

  /* -------------------------- PUBLIC METHODS -------------------------- */

  self.load = function() {
    var startT = Date.now();
    logger.info('Starting TorrentEngine for \'%s - %s\'', torrent.infoHash, torrent.name || '');
    // TODO... make path configurable...
    _engine = torrentStream(torrent.magnetLink, {
      path: '/tmp/torrent-stream/' + torrent.infoHash
    });
    _engine.once('ready', function() {
      if(!torrent.name && _engine.torrent.name)
        torrent.name = _engine.torrent.name;

      logger.info('TorrentEngine ready for \'%s - %s\' (%s ms.)', torrent.infoHash, torrent.name || '', Date.now() - startT);
      _isLoaded = true;
    });
  };

  //---------------------------------------------------------------------------

  self.unload = function() {
    logger.info('Stopping TorrentEngine for \'%s - %s\'', torrent.infoHash, torrent.name || '');
    _engine.destroy();
  };

  //---------------------------------------------------------------------------
  
  self.getFileBytes = function(item, offset, len, buf, cb) {
    var startT = Date.now();
    if (_isLoaded) {
      _doStuff();
    } else {
      _engine.once('ready', _doStuff);
    }
    function _doStuff() {
      _engine.files.forEach(function(file){
        if (file.path === item.torrent.path) {
          var position = 0;
          var end = Math.min(file.length-1, (offset + len-1));
          //var myLen = end-offset;
          //console.log('getFileBytesByRange', path, offset, myLen);
          file.createReadStream({
            start: offset,
            end: end
          })
            .on('data', function(chunk){
              logger.silly('getFileBytes::chunk', item.torrent.path, offset, end, len, end-offset, position, chunk.length);
              chunk.copy(buf, position);
              position += chunk.length;
            })
            .on('end',function(){
              logger.debug('getFileBytes::end', item.torrent.path, offset, end, end-offset, Date.now() - startT);
              cb(null, position);
            })
            .on('error', function(err){
              cb(err);
            });

          // Read-ahead
          var readAheadEnd = Math.min(file.length-1, (offset + Math.ceil(file.length / 200)));
          // TODO... find better way to handle this...
          file.createReadStream({
            start: offset,
            end: readAheadEnd
          });
        }
      })
    }
  };  

}

//---------------------------------------------------------------------------

module.exports = TorrentEngine;
