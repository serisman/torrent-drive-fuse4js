var utils = require('./utils.js');

var f4js = require('fuse4js');

var TorrentDrive = require('./torrent_drive.js');

//---------------------------------------------------------------------------

function TorrentFS(logger) {
  var self = this;

  var _torrentDrive;

  var _mountpoint;
  var _mounted = false;

  /* -------------------------- PUBLIC METHODS -------------------------- */

  self.load = function(cb) {
    _torrentDrive = new TorrentDrive(logger);
    _torrentDrive.load(function(err) {
      if (err) { return cb(err); }

      cb(null);
    });
  };

  //---------------------------------------------------------------------------

  self.addTorrent = function(folder, magnetLink, cb) {
    // TODO... need some error handling...
    var folderItem = _torrentDrive.getItemByPath('/' + folder);
    _torrentDrive.addMagnetLink(folderItem, magnetLink, cb);
  };

  //---------------------------------------------------------------------------

  self.mount = function(mountpoint, cb) {
    var handlers = {
      init: _init,
      destroy: _destroy,
      statfs: _statfs,
      getattr: _getattr,
      mkdir: _mkdir,
      readdir: _readdir,
      open: _open,
      read: _read,
      release: _release
    };

    var opts = ['-o','allow_other'];
    var debugFuse = false;

    try {
      f4js.start(mountpoint, handlers, debugFuse, opts);
    } catch (err) {
      cb(err);
    }

    _mountpoint = mountpoint;
    cb(null);
  };

  /* -------------------------- PRIVATE METHODS -------------------------- */

  /*
   * Handler for the init() FUSE hook. You can initialize your file system here.
   * cb: a callback to call when you're done initializing. It takes no arguments.
   */
  function _init(cb) {
    _mounted = true;

    logger.info('Filesystem mounted at \'%s\'',_mountpoint);
    logger.info("To stop it, type this in another shell: fusermount -u " + _mountpoint);

    cb();
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the destroy() FUSE hook. You can perform clean up tasks here.
   * cb: a callback to call when you're done. It takes no arguments.
   */
  function _destroy(cb) {
    if (_mounted) {
      _torrentDrive.saveConfig(function(err){
        if (err) { return cb(err); }

        logger.info('Filesystem stopped (mount point: \'%s\')', _mountpoint);
        cb();
      });
    } else {
      cb();
    }
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the statfs() FUSE hook.
   * cb: a callback of the form cb(err, stat), where err is the Posix return code
   *     and stat is the result in the form of a statvfs structure (when err === 0)
   */
  function _statfs(cb) {
    logger.silly('statfs');
    // TODO... actually implement this.
    var frsize = 128 * 1024;  // 128 KB
    //var frsize = 512;
    var totalSize = _torrentDrive.getTotalSize();
    cb(0, {
      bsize: frsize,    // Preferred file system block size
      frsize: frsize,   // Fundamental file system block size
      blocks: Math.ceil(totalSize / frsize),  // Total number of block f_frsize in the file system.
      bfree: 0,         // Total number of free blocks of f_frsize in the file system.
      bavail: 0,        // Total number of available blocks of f_frsize that can be used by users without root access.
      files: 1000000,   // Total number of file nodes in the file system
      ffree: 0,         // Number of free file nodes in the file system.
      favail: 0,        // Number of free file nodes that can be user without root access.
      fsid: 0,          // File system ID.
      flag: 1,          // File system flags: [ST_RDONLY = 1, ST_NOSUID = 2, ST_NODEV = 4]
      namemax: 1000000  // Maximum length of a component name for this file system
    });
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the getattr() system call.
   * path: the path to the file
   * cb: a callback of the form cb(err, stat), where err is the Posix return code
   *     and stat is the result in the form of a stat structure (when err === 0)
   */
  function _getattr(path, cb) {
    logger.silly('getattr', path);
    var item = _torrentDrive.getItemByPath(path);
    if (!item) { return cb(-2); } // -ENOENT

    var stat = {};
    //stat.blksize = 128 * 1024;  // 128 KB
    //stat.blksize = 512;

    /*
    if (item.lastViewedByMeDate)
      stat.atime = new Date(item.lastViewedByMeDate);
    if (item.modifiedDate)
      stat.mtime = new Date(item.modifiedDate);
    */

    // http://www.onlineconversion.com/html_chmod_calculator.htm
    switch (item.type) {
      case 'folder':
        var childItems = _torrentDrive.getChildItems(item);
        stat.type = 0040000; // TODO... verify this.
        stat.size = 4096;   // standard size of a directory
        //stat.blocks = Math.ceil(stat.size / 512);
        stat.nlink = childItems.length + 2; // TODO... verify this.
        stat.mode = 040555; // directory with 555 permissions
        break;

      case 'file':
        stat.type = 0100000; // TODO... verify this.
        stat.size = item.fileSize || 0;
        //stat.blocks = Math.ceil(stat.size / 512);
        stat.nlink = 1;
        stat.mode = 0100444; // file with 444 permissions
        break;
    }
    cb(0, stat);
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the mkdir() system call.
   * path: the path of the new directory
   * mode: the desired permissions of the new directory
   * cb: a callback of the form cb(err), where err is the Posix return code.
   */
  function _mkdir(path, mode, cb) {
    logger.silly('mkdir', path);
    /*
    var err = -2; // -ENOENT assume failure
    var dst = lookup(obj, path), dest;
    if (typeof dst.node === 'undefined' && dst.parent != null) {
      dst.parent[dst.name] = {};
      err = 0;
    }
    cb(err);
    */

    // TODO... implement this...

    cb(0);
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the readdir() system call.
   * path: the path to the file
   * cb: a callback of the form cb(err, names), where err is the Posix return code
   *     and names is the result in the form of an array of file names (when err === 0).
   */
  function _readdir(path, cb) {
    logger.silly('readdir', path);
    var item = _torrentDrive.getItemByPath(path);
    if (!item) { return cb(-2); } // -ENOENT
    if (item.type !== "folder") { return cb(-22); } // -EINVAL

    var children = ['.', '..'];
    var childItems = _torrentDrive.getChildItems(item);
    childItems.forEach(function(item){
      children.push(item.name);
    });

    cb(0, children);
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the open() system call.
   * path: the path to the file
   * flags: requested access flags as documented in open(2)
   * cb: a callback of the form cb(err, [fh]), where err is the Posix return code
   *     and fh is an optional numerical file handle, which is passed to subsequent
   *     read(), write(), and release() calls.
   */
  function _open(path, flags, cb) {
    logger.silly('open', path);
    var item = _torrentDrive.getItemByPath(path);
    if (!item) { return cb(-2); } // -ENOENT

    cb(0); // we don't return a file handle, so fuse4js will initialize it to 0
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the read() system call.
   * path: the path to the file
   * offset: the file offset to read from
   * len: the number of bytes to read
   * buf: the Buffer to write the data to
   * fh:  the optional file handle originally returned by open(), or 0 if it wasn't
   * cb: a callback of the form cb(err), where err is the Posix return code.
   *     A positive value represents the number of bytes actually read.
   */
  function _read(path, offset, len, buf, fh, cb) {
    logger.silly('read', path);
    var item = _torrentDrive.getItemByPath(path);
    if (!item) { return cb(-2); } // -ENOENT
    if (item.type !== "file") { return cb(-1); } // -EPERM

    _torrentDrive.getFileBytes(item, offset, len, buf, function(err, len) {
      if (err) { console.error(err); return cb(-16); } // -EBUSY

      cb(len);
    });
  }

  //---------------------------------------------------------------------------

  /*
   * Handler for the release() system call.
   * path: the path to the file
   * fh:  the optional file handle originally returned by open(), or 0 if it wasn't
   * cb: a callback of the form cb(err), where err is the Posix return code.
   */
  function _release(path, fh, cb) {
    logger.silly('release', path);
    cb(0);
  }
}

//---------------------------------------------------------------------------

module.exports = TorrentFS;
