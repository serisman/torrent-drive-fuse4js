var utils = require('./utils.js');
var fs = require('fs');
var path = require('path');

var TorrentEngine = require('./torrent_engine.js');

//---------------------------------------------------------------------------

function TorrentDrive(logger) {
  var self = this;

  var _rootConfigPath = path.join(__dirname, "../.config");
  var _configFile = path.join(_rootConfigPath, 'torrent_drive.config.json');
  var _config = {
    items: {},
    torrents: {}
  };
  var _pathIndex = {};
  var _childItems = {};
  var _totalSize = 0;

  /* -------------------------- PUBLIC METHODS -------------------------- */

  self.load = function(cb) {
    _loadConfig(function(err) {
      if (err && (!err.code || err.code !== 'ENOENT')) { return cb(err); }

      _buildIndexes();

      cb(null);
    });
  };

  //---------------------------------------------------------------------------

  // TODO... clean this up...
  self.addMagnetLink = function(parentItem, magnetLink, cb) {
    var torrentStream = require('torrent-stream');
    var uuid = require('uuid');
    
    var engine = torrentStream(magnetLink);
    engine.on('ready', function() {
      var infoHash = engine.torrent.infoHash;
      var name = engine.torrent.name;
      var torrent = _config.torrents[infoHash] = {
        infoHash: infoHash,
        name: name,
        magnetLink: magnetLink,
        files: {}
      };
      engine.files.forEach(function (file) {
        var path = file.path;
        var name = file.name;
        var fileSize = file.length;

        torrent.files[path] = {
          path: path,
          name: name,
          fileSize: fileSize
        };

        var file_id = uuid.v4();
        var fileItem = _config.items[file_id] = {
          id: file_id,
          type: 'file',
          name: name,
          parent_ids: [],
          fileSize: fileSize,
          torrent: {
            infoHash: infoHash,
            path: path
          }
        };

        var parent = parentItem;
        var pathParts = path.split('/');
        for (; pathParts.length > 0;) {
          var pathPart = pathParts.shift();
          //console.log('pathPart', pathPart);
          if (pathParts.length === 0) {
            fileItem.parent_ids.push(parent.id);
          } else {
            var foundItem = null;
            var parentChildren = _childItems[parent.id];
            if (!parentChildren) {
              parentChildren = [];
              _childItems[parent.id] = parentChildren;
            }
            //console.log('parent\'s children', parentChildren);
            parentChildren.forEach(function (child) {
              if (child.name === pathPart) {
                //console.log('existing folder', child);
                foundItem = child;
              }
            });
            if (!foundItem) {
              // create and add to indexes
              var folder_id = uuid.v4();
              //console.log('new folder', folder_id);
              foundItem = _config.items[folder_id] = {
                id: folder_id,
                type: 'folder',
                name: pathPart,
                parent_ids: [ parent.id ]
              };
              _childItems[folder_id] = [];
              parentChildren.push(foundItem);
              //console.log('new folder', foundItem);
            }
            parent = foundItem;
          }
        }

      });
      //console.log('_config', _config);
      _buildIndexes();
      _saveConfig(cb);
      //cb(null);
    });
  };

  //---------------------------------------------------------------------------

  self.getItemByPath = function(path) {
    return _pathIndex[path];
  };

  //---------------------------------------------------------------------------

  self.getChildItems = function(item) {
    return _childItems[item.id] || [];
  };

  //---------------------------------------------------------------------------

  self.getTotalSize = function() {
    return _totalSize;
  };

  //---------------------------------------------------------------------------

  self.getFileBytes = function(item, offset, len, buf, cb) {
    var torrent = _config.torrents[item.torrent.infoHash];
    var torrentEngine = _getTorrentEngine(torrent);
    torrentEngine.getFileBytes(item, offset, len, buf, cb);
  };

  /* -------------------------- PRIVATE METHODS -------------------------- */

  var _torrentEngines = {};
  var TORRENT_ENGINE_TIMEOUT_MINUTES = 10;

  function _getTorrentEngine(torrent) {
    var torrentEngine = _torrentEngines[torrent.infoHash];
    if (!torrentEngine) {
      torrentEngine = new TorrentEngine(torrent, logger);
      torrentEngine.load();
      _torrentEngines[torrent.infoHash] = torrentEngine;
    }
    if (torrentEngine.timeoutId)
      clearTimeout(torrentEngine.timeoutId);
    torrentEngine.timeoutId = setTimeout(function() {
      delete torrentEngine.timeoutId;
      delete _torrentEngines[torrent.infoHash];
      torrentEngine.unload();
    }, TORRENT_ENGINE_TIMEOUT_MINUTES * 60 * 1000);

    return torrentEngine;
  }

  //---------------------------------------------------------------------------

  function _buildIndexes() {
    var childItems = {};
    var pathIndex = {};
    var rootItem = { id: 'root', type: 'folder' };
    pathIndex['/'] = rootItem;
    var orphanCheck = {};

    var addChildItem = function(parent, item) {
      var children = childItems[parent.id];
      if (!children) {
        children = [];
        childItems[parent.id] = children;
      }
      if (children.indexOf(item) === -1)
        children.push(item);
    };

    var getParentPaths = function(item) {
      if (!item.parent_ids || item.parent_ids.length === 0) {
        if (!orphanCheck[item.id]) {
          logger.debug('orphan:', item.id + ' - ' + item.name);
          orphanCheck[item.id] = true;
        }
        //addChildItem(orphans, item);
        //addChildItem(rootItem, orphans);
        //pathIndex['/.orphans'] = orphans;
        //return ["/.orphans"];
        return [];
      } else {
        var paths = [];
        item.parent_ids.forEach(function (parent_id) {
          //console.log('parent_id',parent_id);
          if (parent_id === 'root') {
            addChildItem(rootItem, item);
            paths.push("");
          } else {
            var parentItem = _config.items[parent_id];
            if (!parentItem) {
              logger.warn('parent item not found: ', item.id  + ' - ' + item.name + ' - ' + parent_id);
            } else {
              addChildItem(parentItem, item);

              var parentPaths = getParentPaths(parentItem);
              for (var i = 0; i < parentPaths.length; i++) {
                paths.push(parentPaths[i] + '/' + parentItem.name);
              }
            }
          }
        });
        return paths;
      }
    };

    var totalSize = 0;

    for (var key in _config.items) {
      var item = _config.items[key];

      if (item.fileSize) {
        totalSize += new Number(item.fileSize);
      }

      var parentPaths = getParentPaths(item);
      parentPaths.forEach(function(parentPath) {
        var path = parentPath + '/' + item.name;
        pathIndex[path] = item;
      });
    }

    _pathIndex = pathIndex;
    _childItems = childItems;
    _totalSize = totalSize;

    //console.log('pathIndex', pathIndex);
    //console.log('childItems', childItems);

    logger.info('Finished indexing Drive metadata. (referencing %s)', totalSize.bytesToSize());
  }

  //---------------------------------------------------------------------------

  function _loadConfig(cb) {
    fs.readFile(_configFile, 'utf8', function (err, data) {
      if (err) { return cb(err); }
      try {
        _config = JSON.parse(data);
      } catch (err) {
        return cb(err);
      }
      cb(null);
    });
  }

  //---------------------------------------------------------------------------

  function _saveConfig(cb) {
    utils.mkdir(_rootConfigPath, function(err) {
      if (err) { return cb(err); }

      fs.writeFile(_configFile, JSON.stringify(_config, null, '  '), 'utf8', function (err) {
        if (err) { return cb(err); }
        cb(null);
      });
    });
  }
  self.saveConfig = _saveConfig;
}

//---------------------------------------------------------------------------

module.exports = TorrentDrive;
