var winston = require('winston');
var path = require('path');
var util = require('util');

var utils = require('./lib/utils.js');
var HealthLogger = require('./lib/health_logger.js');
var TorrentFS = require('./lib/torrent_fs.js');

// TODO... make this configurable...
var mountpoint = '/mnt/torrents';

function errorHandler(err) {
  if (err) {
    console.error(err);
    process.exit(-1);
  }
}

var rootLoggingPath = path.join(__dirname, "logs");
utils.mkdir(rootLoggingPath, function(err) {
  errorHandler(err);

  var logger = new(winston.Logger)({
    exitOnError: false,
    transports: [
      new (winston.transports.Console)({
        level: 'info',
        colorize: true,
        timestamp: function () { return new Date().format("yyyy-MM-dd h:mm:ss"); }
      }),
      new(winston.transports.File)({
        level: 'debug',
        filename: 'logger.log',
        dirname: rootLoggingPath,
        maxSize: 1024*1024,
        maxFiles: 10,
        json: false,
        timestamp: function () { return new Date().format("yyyy-MM-dd h:mm:ss"); }
      })
    ]
  });

  var torrentFS = new TorrentFS(logger);
  torrentFS.load(function(err) {
    errorHandler(err);

    var args = process.argv;
    if (args.length === 4) {
      var folder = args[2];
    var magnetLink = args[3];
      torrentFS.addTorrent(folder, magnetLink, function(err) {
        errorHandler(err);

        console.log('Added torrent');
        process.exit(0);
      });
    } else {
      torrentFS.mount(mountpoint, function(err) {
        errorHandler(err);

        var healthLogger = new HealthLogger(rootLoggingPath);
        healthLogger.start();

      });
    }
  });
});
